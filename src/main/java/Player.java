import java.util.ArrayList;

public class Player {
    int pos;
    int money;

    public Player() {
    }

    public Player(int startPos, int startMoney){
        this.pos = startPos;
        this.money = startMoney;
    }

    public void play(ArrayList<Player> players, Dice d6){
        this.pos += d6.roll();

    }
}
